package eu.niko.pluginmanager;

import android.support.v4.app.Fragment;

public abstract class PluginActivity extends Fragment implements IClassLoader {
    @Override
    public Object loadedClass(Object... parentActivity) {
        return this;
    }

}
