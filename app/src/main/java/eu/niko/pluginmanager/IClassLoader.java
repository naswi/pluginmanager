package eu.niko.pluginmanager;

public interface IClassLoader {
    Object loadedClass(Object... parentActivity);
    Object talkToMe(Object... messages);
}
