package eu.niko.pluginmanager;

public interface IParentActivity {
    void talkToMe(Object... object);
    void freeResource();
    void startActivity(Object... objects);
    IViewAdapter setViewAdapter();
}
