package eu.niko.plugin_manager.ControlUiHandler;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.sdsmdg.harjot.vectormaster.models.GroupModel;
import com.sdsmdg.harjot.vectormaster.models.PathModel;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import eu.niko.naswi.aidl.Property;
import eu.niko.plugin_manager.FragmentHandler;
import eu.niko.plugin_manager.CustomImageView;
import eu.niko.plugin_manager.PluginMain;
import eu.niko.plugin_manager.plugindrawhandler.Utility;
import eu.niko.pluginmanager.IParentActivity;

public class SwipeControl extends PlayerAbstract implements View.OnTouchListener {
    GestureDetector gestureDetector;
    private int resId, vectorId;
    private String status = "0";
    FragmentHandler fragmentHandler;
    private float densityDpi;
    Timer time = new Timer();
    long idleTime = new Date().getTime();


    private Handler sendHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            Property[] properties = (Property[]) message.obj; //new Property("Brightness" , status);
            fragmentHandler.talkToMe(action , properties);
            return false;
        }
    });

    public SwipeControl(View containerView) {
        super(containerView);
    }

    @Override
    protected void doAction(Property[] eventName) {

        status = getProp(eventName , "Status");
        String b = getProp(eventName , "Brightness");
        brightness = b.equals("") ? brightness : b;
        try {
            String currResp = status;
            if (status.equals("Off"))
                for (Utility.Response response : this.control.getResponses()
                        ) {
                    if (response.getName().equals(status)) {
                        currResp = response.getName();
                        if (!response.getPathModel().getName().isEmpty()) {

                            PathModel pathModel = vectorMasterDrawable.getPathModelByName(response
                                    .getPathModel().getName());
                            Paint paint = new Paint();
                            paint.setColor(Color.parseColor(response.getPathModel().getColor()));
                            pathModel.setPathPaint(paint);
                            vectorMasterDrawable.update();

                        }

                        if (!response.getGroupModel().getName().isEmpty()) {
                            GroupModel groupModel = vectorMasterDrawable.getGroupModelByName(response
                                    .getGroupModel().getName());
                            for (PathModel pathModel1 : groupModel.getPathModels()) {
                                Paint paint = new Paint();
                                paint.setColor(Color.parseColor(response.getGroupModel().getColor()));
                                pathModel1.setPathPaint(paint);
                            }
                            vectorMasterDrawable.update();
                        }
                        break;
                    }

                }
            Log.i("XXXK" , "ok: "
                    + brightness + "000" + currResp +
                    " ");
            if (!brightness.isEmpty() && currResp.equals("On")) {
                float value = Math.round(Float.valueOf(brightness) * 10.0f) / 1000.0f;
                Log.i("XXXFY" , "doAction: " + value + "," + densityDpi);
                Utility.paintGradient(vectorMasterDrawable , getClickableRegion() ,
                        value
                        , densityDpi);
                imageView.setScreenY(value);
                vectorMasterDrawable.update();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void initialize(FragmentHandler fragmentHandler , int resId , int vectorId , Utility.Control
            control
    ) {
        this.resId = resId;
        this.control = control;
        this.vectorId = vectorId;
        initialize(fragmentHandler);
        super.action = fragmentHandler.getAction();
    }

    @Override
    public void initialize(final FragmentHandler fragmentHandler , AppCompatImageView imageView ,
                           int vectorId ,
                           Utility.Control
                                   control) {
        time.scheduleAtFixedRate(new TimerTask() {
            @Override

            public void run() {
                if (((new Date().getTime() - idleTime) / 1000) <= 5)
                    return;
                fragmentHandler.getmParentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Fragment fm = FragmentHandler.newInstance((IParentActivity) fragmentHandler
                                        .getmParentActivity() , action ,
                                fragmentHandler.getBaseControl() , fragmentHandler.getBaseControl
                                        ().getControlList()
                                        .get(0) , fragmentHandler.getFragmentId());
                        PluginMain.getViewAdapter().getFragments().set(fragmentHandler.getFragmentId() ,
                                fm);
                        PluginMain.getViewAdapter().notifyDataSetChanged();
                    }
                });

                time.cancel();
                time = null;
            }
        } , 00 , 500);
        this.imageView = (CustomImageView) imageView;
        this.control = control;

        this.imageView.setText = true;
        this.vectorId = vectorId;
        initialize(fragmentHandler);
        super.action = fragmentHandler.getAction();
    }

    @Override
    public void initialize(FragmentHandler fragmentHandler) {
        dm = fragmentHandler.getActivity().getResources().getDisplayMetrics();
        super.writeText = true;
        super.drawView(resId , vectorId);
        super.defineClickableRegion("xxx");
        gestureDetector = super.defineGesture(new GestureListener(sendHandler , new
                GestureAction[]{GestureAction.singleTap}));
        containerView.setOnTouchListener(this);
        this.fragmentHandler = fragmentHandler;
        dm = fragmentHandler.getActivity().getResources().getDisplayMetrics();

        densityDpi = dm.density;
    }


    @Override
    public boolean onTouch(View view , MotionEvent motionEvent) {
        idleTime = new Date().getTime();
        float x1 = (motionEvent.getX()) * (160f / dm
                .densityDpi);
        float y1 = (motionEvent.getY()) * (160f / dm
                .densityDpi) - getClickableRegion().getBounds().top;

        if (isContainedInRegion(x1 , y1)) {
            int act = motionEvent.getAction();
            switch (motionEvent.getActionMasked()) {
                case MotionEvent.ACTION_MOVE:
                    float getVPPercent = getVerticalPercetageOnArea(y1);
                    Message msg = new Message();
                    Property[] properties = new Property[]{
                            new Property("Brightness" , "" + (int) ((getVPPercent) * 100)) ,
                            new Property("Status" , "On") ,
                    };
                    imageView.setScreenY(getVPPercent);
                    msg.obj = properties;
                    sendHandler.sendMessage(msg);
                    Utility.paintGradient(vectorMasterDrawable , getClickableRegion() , getVPPercent ,
                            densityDpi);
                    vectorMasterDrawable.update();
                    return true;
            }
            return true;
        }

        return true;
    }

}
