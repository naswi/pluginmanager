package eu.niko.plugin_manager.ControlUiHandler;

import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Shader;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import eu.niko.plugin_manager.R;
import eu.niko.pluginmanager.IClassLoader;
import eu.niko.pluginmanager.IParentActivity;
import com.sdsmdg.harjot.vectormaster.VectorMasterDrawable;
import com.sdsmdg.harjot.vectormaster.models.PathModel;

//7D6496
public class LevelControl extends Fragment implements IClassLoader, View.OnTouchListener {
    FragmentActivity mParentActivity;
    Region clickableRegion;
    VectorMasterDrawable vectorMasterDrawable;
    GestureDetector mGestureDetector;


    public static LevelControl newInstance(IParentActivity parentActivity) {

        Bundle args = new Bundle();

        LevelControl fragment = new LevelControl();
        fragment.setArguments(args);
        fragment.loadedClass(parentActivity);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main , null);
        mGestureDetector = new GestureDetector(mParentActivity , new GestureDetector.SimpleOnGestureListener());

        vectorMasterDrawable = new VectorMasterDrawable(mParentActivity , R.drawable.ic_ventilation_level);
        AppCompatImageView view1 = view.findViewById(R.id.vwTest);
        view1.setBackground(vectorMasterDrawable);
        view1.setOnTouchListener(this);
        PathModel pathModel = vectorMasterDrawable.getPathModelByName("xxx");
        clickableRegion = new Region();
        rectF = new RectF();
        Path bigCirclePath =
                pathModel.getPath();
        bigCirclePath.computeBounds(rectF , true);

        Rect rect = new Rect((int) rectF.left , (int) rectF.top , (int) rectF.right , (int) rectF
                .bottom);

        clickableRegion.setPath(bigCirclePath , new Region(rect));


        return view;
    }

    void buttonGradient(final float f) {
        float[] vals = new float[]{0f , .001f , .25f , .251f};
        if (f <= .5f && f > .25f) {
            vals = new float[]{.25f , .251f , .5f , .501f};
        } else if (f <= .75f && f > .5f) {
            vals = new float[]{.5f , .501f , .75f , .751f};
        } else if (f <= 1f && f > .75f) {
            vals = new float[]{.75f , .751f , 1f , 1f};
        }
        DisplayMetrics dm = mParentActivity.getResources().getDisplayMetrics();
        float densityDpi = dm.density;
        PathModel pathModel = vectorMasterDrawable.getPathModelByName("xxx");
        LinearGradient shader = new LinearGradient(0 , 0 , 0 , rectF.height() * dm.density ,
                new int[]{Color.parseColor("#9B82B4") ,
                        Color.parseColor("#7D6496") ,
                        Color.parseColor("#7D6496") ,
                        Color.parseColor("#9B82B4")
                } , vals , Shader
                .TileMode
                .CLAMP);

        Paint paint = new Paint();
        paint.setShader(shader);
        pathModel.setPathPaint(paint);
        vectorMasterDrawable.update();
    }

    RectF rectF;

    public boolean contains(float x , float y) {
        return clickableRegion.contains((int) x , (int) y);
    }

    @Override
    public Object loadedClass(Object... parentActivity) {
        mParentActivity = (FragmentActivity) parentActivity[0];
        return this;
    }

    @Override
    public Object talkToMe(Object... messages) {
        return null;
    }


    @Override
    public boolean onTouch(View view , MotionEvent motionEvent) {
        float Top = rectF.top;
        int vLeft = view.getLeft();
        int vTop = view.getTop();
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();

        DisplayMetrics dm = getResources().getDisplayMetrics();

        float x1 = motionEvent.getX() * (160f / dm.densityDpi);
        float y1 = motionEvent.getY() * (160f / dm.densityDpi);

        if (contains(x1 , y1)) {
            // Toast.makeText(getActivity(),String.format("%s,%s",x1,y1),Toast.LENGTH_SHORT).show();

            float a = y1 / (rectF.bottom - rectF.top);
            scale(a);
            String g = "";
            return false;
        }

        return false;
    }


    void scale(float y) {
        buttonGradient(y);

    }

    boolean isOn = true;
}
