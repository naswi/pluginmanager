package eu.niko.plugin_manager.ControlUiHandler;


import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Region;
import android.graphics.Shader;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.sdsmdg.harjot.vectormaster.models.GroupModel;
import com.sdsmdg.harjot.vectormaster.models.PathModel;

import eu.niko.naswi.aidl.Property;
import eu.niko.plugin_manager.FragmentHandler;
import eu.niko.plugin_manager.CustomImageView;
import eu.niko.plugin_manager.PluginMain;
import eu.niko.plugin_manager.plugindrawhandler.Utility;
import eu.niko.pluginmanager.IParentActivity;


public class TapControl extends PlayerAbstract implements View.OnTouchListener {
    GestureDetector gestureDetector;
    private int resId, vectorId;
    private String status = "On";
    FragmentHandler fragmentHandler;


    private Handler sendHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            Object[] objects = (Object[]) Message.obtain(message).obj;
            GestureAction gestureAction = (GestureAction) objects[1];
            MotionEvent motionEvent = (MotionEvent) objects[0];
            float x1 = motionEvent.getX() * (160f / dm.densityDpi);
            float y1 = motionEvent.getY() * (160f / dm.densityDpi);
            if (isContainedInRegion((int) x1 , (int) y1)) {
                Property property = new Property("Status" ,
                        (status.equals("On") ? "Off" : "On"));
                fragmentHandler.talkToMe(action , new Property[]{property});
            } else {
                if (gestureAction == GestureAction.longPress && fragmentHandler.getBaseControl()
                        .isHasLevel()) {
                    Log.i("XXXFN" , "Activity starts here...");

                    Fragment fm = FragmentHandler.newInstance((IParentActivity) fragmentHandler
                                    .getmParentActivity() , action ,
                            fragmentHandler.getBaseControl() , fragmentHandler.getBaseControl().getControlList()
                                    .get(1) , fragmentHandler.getFragmentId());
                    PluginMain.getViewAdapter().getFragments().set(fragmentHandler.getFragmentId() , fm);
                    PluginMain.getViewAdapter().notifyDataSetChanged();


                }
            }
            return false;
        }
    });

    public TapControl(View view) {
        super(view);
    }


    @Override
    public void initialize(FragmentHandler fragmentHandler , int resId , int vectorId , Utility.Control control) {
        this.resId = resId;
        this.vectorId = vectorId;
        this.control = control;
        initialize(fragmentHandler);
        super.action = fragmentHandler.getAction();
        dm = fragmentHandler.getActivity().getResources().getDisplayMetrics();

        densityDpi = dm.density;
    }

    @Override
    public void initialize(FragmentHandler fragmentHandler , AppCompatImageView imageView , int vectorId ,
                           Utility.Control control) {
        this.imageView = (CustomImageView) imageView;
        this.vectorId = vectorId;
        this.control = control;
        initialize(fragmentHandler);
        super.action = fragmentHandler.getAction();
        dm = fragmentHandler.getActivity().getResources().getDisplayMetrics();

        densityDpi = dm.density;
    }

    @Override
    public void initialize(FragmentHandler fragmentHandler) {
        dm = fragmentHandler.getActivity().getResources().getDisplayMetrics();

        super.drawView(resId , vectorId);
        super.defineClickableRegion(this.control.getDrawer().getActiveRegion());
        gestureDetector = super.defineGesture(new GestureListener(sendHandler , new
                GestureAction[]{GestureAction.singleTap , GestureAction.longPress , GestureAction.fling}));
        containerView.setOnTouchListener(this);
        this.fragmentHandler = fragmentHandler;
    }



    @Override
    protected void doAction(Property[] eventName) {
        status = getProp(eventName , "Status");
        String b = getProp(eventName , "Brightness");
        brightness = b.equals("") ? brightness : b;
        try {
            String currResp="";
            for (Utility.Response response : this.control.getResponses()
                    ) {
                if (response.getName().equals(status)) {
                    currResp=response.getName();
                    if (!response.getPathModel().getName().isEmpty()) {
                        Log.i("XXXG" , "doAction: "
                                + response.getPathModel().getName() + " " + response.getPathModel
                                ().getColor() +
                                " " + response.getName());
                        PathModel pathModel = vectorMasterDrawable.getPathModelByName(response
                                .getPathModel().getName());
                        Paint paint=new Paint();
                        paint.setColor(Color.parseColor(response.getPathModel().getName()));
                        pathModel.setPathPaint(paint);
                        vectorMasterDrawable.update();
                    }

                    if (!response.getGroupModel().getName().isEmpty()) {
                        GroupModel groupModel = vectorMasterDrawable.getGroupModelByName(response
                                .getGroupModel().getName());
                        for (PathModel pathModel1 : groupModel.getPathModels()) {
                            Paint paint=new Paint();
                            paint.setColor(Color.parseColor(response.getGroupModel().getColor()));
                            pathModel1.setPathPaint(paint);
                        }
                        vectorMasterDrawable.update();
                    }
                    break;
                }

            }

            if (!brightness.isEmpty() && currResp.equals("On")) {
                float value = Math.round(Float.valueOf(brightness) * 10.0f) / 1000.0f;
                Log.i("XXXFY" , "doAction: " + value + "," + densityDpi);
                Paint paint = paintGradient(getClickableRegion() ,
                        value
                        , densityDpi);
                PathModel pathModel = vectorMasterDrawable.getPathModelByName(control.getDrawer()
                        .getActiveRegion());
                pathModel.getPathPaint().getColor();
                pathModel.setPathPaint(paint);
                vectorMasterDrawable.update();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    protected Paint paintGradient(Region region ,
                                  float
                                          percent ,
                                  float densityDpi) {
        try {

            LinearGradient shader = new LinearGradient(0 , 0 , 0 , region
                    .getBounds().height()
                    * densityDpi ,
                    new int[]{Color.parseColor("#FDDE92") ,
                            Color.parseColor("#FFC73C")} , new float[]{(percent -
                    .001f)//-.01f
                    // for
                    // space
                    // between gradient
                    , percent} , Shader
                    .TileMode
                    .CLAMP);
            Log.i("XXXOF" , "doAction: " + percent);
            Paint paint = new Paint();
            paint.setShader(shader);
            return paint;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean onTouch(View view , MotionEvent motionEvent) {
        return gestureDetector.onTouchEvent(motionEvent);
    }
}
