package eu.niko.plugin_manager.ControlUiHandler;

import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.MotionEvent;

import java.util.Arrays;
import java.util.List;

public class GestureListener extends GestureDetector.SimpleOnGestureListener {

    static String currentGestureDetected;
    private final Handler handler;
    private List<GestureAction> conditions;

    public GestureListener(Handler handler , GestureAction[] conditions) {
        this.conditions = Arrays.asList(conditions);
        this.handler = handler;
    }

    void runCondition(MotionEvent motionEvent , GestureAction eventType) {
        Message message = new Message();
        message.obj = new Object[]{motionEvent , eventType};

        if (conditions.contains(eventType))
            this.handler.sendMessage(message);

    }

    // Override s all the callback methods of GestureDetector.SimpleOnGestureListener
    @Override
    public boolean onSingleTapUp(MotionEvent ev) {
        runCondition(ev,GestureAction.singleTap);
        return true;
    }

    @Override
    public void onShowPress(MotionEvent ev) {
        runCondition(ev,GestureAction.showPress);

    }

    @Override
    public void onLongPress(MotionEvent ev) {
        runCondition(ev,GestureAction.longPress);

    }

    @Override
    public boolean onScroll(MotionEvent e1 , MotionEvent e2 , float distanceX , float distanceY) {
        runCondition(e1,GestureAction.scroll);

        return true;
    }

    @Override
    public boolean onDown(MotionEvent ev) {

        runCondition(ev,GestureAction.down);

        return true;
    }


    @Override
    public boolean onFling(MotionEvent e1 , MotionEvent e2 , float velocityX , float velocityY) {
        float sensitvity = 50;
        int x = -1;
        if ((e1.getY() - e2.getY()) > sensitvity) {
            x = 1;
        } else if ((e2.getY() - e1.getY()) > sensitvity) {
            x = 0;
        }
        currentGestureDetected = e1.getAction() + "  " + e2.getAction();
        runCondition(e1,GestureAction.fling);

        return true;
    }

}
enum GestureAction{
    singleTap,showPress,longPress,scroll,down,fling
}
