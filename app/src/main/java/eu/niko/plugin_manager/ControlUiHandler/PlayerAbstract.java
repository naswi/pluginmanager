package eu.niko.plugin_manager.ControlUiHandler;

import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.View;

import com.sdsmdg.harjot.vectormaster.VectorMasterDrawable;
import com.sdsmdg.harjot.vectormaster.models.PathModel;

import eu.niko.naswi.aidl.Action;
import eu.niko.naswi.aidl.Property;
import eu.niko.plugin_manager.IPlayer;
import eu.niko.plugin_manager.CustomImageView;
import eu.niko.plugin_manager.plugindrawhandler.Utility;


public abstract class PlayerAbstract implements IPlayer {
    protected Utility.Control control;
    protected String brightness="";
    protected DisplayMetrics dm;
    protected float densityDpi;
    protected Action action;
    protected View containerView;
    protected CustomImageView imageView;
    protected VectorMasterDrawable vectorMasterDrawable;
    private Region clickableRegion;
    protected boolean writeText=false;
    protected GestureDetector gestureDetector;

    public Region getClickableRegion() {
        return clickableRegion;
    }

    public Handler getHandler() {
        return handler;
    }

    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            doAction((Property[])msg.obj);
            return false;
        }
    });

    protected abstract void doAction(Property[] eventName);

    public boolean isContainedInRegion(float x , float y) {
        return clickableRegion.contains((int) x , (int) y);
    }

    public PlayerAbstract(View containerView) {
        this.containerView = containerView;
    }


    public float getVerticalPercetageOnArea(float y) {
        float a = y / (clickableRegion.getBounds().bottom  -
                clickableRegion.getBounds().top );
        return a;
    }


    @Override
    public View drawView(int drawingRegionId , int vectorId) {
        vectorMasterDrawable = new VectorMasterDrawable(containerView.getContext() , vectorId);
        if (imageView == null)
            imageView = containerView.findViewById(drawingRegionId);
        imageView.setBackground(vectorMasterDrawable);
        return containerView;
    }

    String getProp(Property[] properties , String propName) {
        String val = "";
        for (Property prop : properties) {
            if (prop.getName().equals(propName)) {
                val = prop.getValue();
                break;
            }

        }
        return val;
    }
    @Override
    public GestureDetector defineGesture(GestureListener gestureListener) {
        gestureDetector = new GestureDetector(containerView.getContext() , gestureListener);
        return gestureDetector;
    }

    @Override
    public Region defineClickableRegion(String clickableVectorPath) {
        PathModel pathModel = vectorMasterDrawable.getPathModelByName(clickableVectorPath);
        clickableRegion = new Region();
        RectF rectF = new RectF();
        Path bigCirclePath =
                pathModel.getPath();
        bigCirclePath.computeBounds(rectF , true);
        Rect rect = new Rect((int) rectF.left , (int) rectF.top , (int) rectF.right , (int) rectF
                .bottom);
        clickableRegion.setPath(bigCirclePath , new Region(rect));
        return clickableRegion;
    }
}
