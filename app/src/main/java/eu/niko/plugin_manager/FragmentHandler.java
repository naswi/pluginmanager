package eu.niko.plugin_manager;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import eu.niko.naswi.aidl.Action;
import eu.niko.naswi.aidl.Property;

import eu.niko.plugin_manager.ControlUiHandler.PlayerAbstract;
import eu.niko.plugin_manager.plugindrawhandler.Utility;
import eu.niko.plugin_manager.plugininitialization.InitFragmentHandler;
import eu.niko.pluginmanager.IClassLoader;
import eu.niko.pluginmanager.IParentActivity;

public class FragmentHandler extends Fragment implements IClassLoader {
    FragmentActivity mParentActivity;
    IPlayer playerControl;
    Handler fragmentActivityHandler;
    Action action;
    Utility.BaseControl baseControl;
    Utility.Control control;
    int fragmentId = -1;



    public Action getAction() {
        return action;
    }

    public Utility.Control getControl() {
        return control;
    }

    public FragmentActivity getmParentActivity() {
        return mParentActivity;
    }

    public int getFragmentId() {
        return fragmentId;
    }

    public IPlayer getPlayerControl() {
        return playerControl;
    }


    public Utility.BaseControl getBaseControl() {
        return baseControl;
    }

    public static FragmentHandler newInstance(IParentActivity fragmentActivity , Action action ,
                                              int fragmentId
    ) {

        Bundle args = new Bundle();
        args.putParcelable("action" , action);
        FragmentHandler fragment = new FragmentHandler();
        fragment.setArguments(args);
        fragment.loadedClass(fragmentActivity);
        return fragment;
    }

    private static Bundle putInArgs(Action action ,
                                    Utility.BaseControl baseControl , Utility.Control kontrol , int fragmentId) {
        Bundle args = new Bundle();
        args.putInt("fragmentId" , fragmentId);
        args.putParcelable("action" , action);
        args.putSerializable("baseControl" , baseControl);
        args.putSerializable("control" , kontrol);
        return args;
    }

    public static FragmentHandler newInstance(IParentActivity fragmentActivity , Action action ,
                                              Utility.BaseControl baseControl , Utility.Control kontrol , int fragmentId) {
        Bundle args = putInArgs(action , baseControl , kontrol , fragmentId);
        FragmentHandler fragment = new FragmentHandler();
        fragment.setArguments(args);
        fragment.loadedClass(fragmentActivity);
        return fragment;
    }

    int getResourceId(String name , String type , String packageName) {
        return getResources().getIdentifier(name , type , packageName);
    }

    void initInfos(Bundle bundle) {
        this.baseControl = (Utility.BaseControl) bundle.getSerializable("baseControl");
        this.control = (Utility.Control) bundle.getSerializable("control");
        this.action = bundle.getParcelable("action");
        this.fragmentId = bundle.getInt("fragmentId");
    }

    void createInstanceOfPlayer(ConstraintLayout constraintLayout) {
        try {
            Class genericClass = Class.forName(this.control.getClassName());
            Constructor constructor = genericClass.getDeclaredConstructor(new Class[]{View.class});
            playerControl = IPlayer.class.cast(constructor.newInstance(new
                    Object[]{constraintLayout}));
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main , null);
        ConstraintLayout constraintLayout = (ConstraintLayout) view;
        CustomImageView imageView_cust = InitFragmentHandler.createNewImageView(mParentActivity ,
                constraintLayout);
        initInfos(getArguments());
        createInstanceOfPlayer(constraintLayout);
        int drawerId = getResourceId(this.control.getDrawer().getDrawableId() , "drawable" , this
                .getClass().getPackage().getName());
        playerControl.initialize(this , imageView_cust , drawerId , control);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        doAction(this.baseControl.getProperties());
    }

    public void doAction(Property[] msg) {
        if (msg != null && msg.length > 0) {
            this.baseControl.setProperties(msg);
            Log.i("XXXX2" , String.format(" sent: %s" , msg));
            PlayerAbstract playerAbstract = (PlayerAbstract) playerControl;
            Message message = new Message();
            message.obj = msg;
            playerAbstract.getHandler().sendMessage(message);
        }

    }

    @Override
    public Object loadedClass(Object... parentActivity) {
        mParentActivity = (FragmentActivity) parentActivity[0];
        return this;
    }

    @Override
    public Object talkToMe(Object... messages) {
        if (fragmentActivityHandler == null) {
            Log.i("XXX7" , "talkToMe: " + messages);
            Class<?> clazz = mParentActivity.getClass();
            Field field;
            try {
                field = clazz.getDeclaredField("handler");
                field.setAccessible(true);
                fragmentActivityHandler = Handler.class.cast(field.get(mParentActivity));
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        Message msg = new Message();
        msg.obj = messages;
        fragmentActivityHandler.sendMessage(msg);


        return null;
    }

}
