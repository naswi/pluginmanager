package eu.niko.plugin_manager;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class CustomImageView extends android.support.v7.widget.AppCompatImageView {

    private float screenY=1.02f;
    public boolean setText=false;
    Context context;

    public void setScreenY(float screenY) {
        this.screenY = screenY;
    }

    public CustomImageView(Context context) {
        super(context, (AttributeSet)null);this.context=context;
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs, 0);this.context=context;
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
         super(context,attrs,defStyleAttr);
         this.context=context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(setText) {
            Paint textPaint = new Paint();
            textPaint.setTextSize(85.0f);
            textPaint.setTextAlign(Paint.Align.CENTER);
            Typeface typeface = Typeface.createFromAsset(context.getAssets() ,
                    "fonts/roboto_medium.ttf");
            textPaint.setTypeface(typeface);
            int xPos = (canvas.getWidth() / 2);
            int yPos = (int) ((canvas.getHeight() / 2) - ((textPaint.descent() + textPaint.ascent()) / 2));

            canvas.drawText((102 - (int) (screenY * 100)) + "" , xPos , yPos , textPaint);
        }
    }
}
