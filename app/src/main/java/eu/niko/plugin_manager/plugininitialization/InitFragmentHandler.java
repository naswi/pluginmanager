package eu.niko.plugin_manager.plugininitialization;

import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.view.ViewGroup;

import eu.niko.plugin_manager.CustomImageView;

public class InitFragmentHandler {

   public static CustomImageView createNewImageView(FragmentActivity mParentActivity, ConstraintLayout
            constraintLayout
    ){
        CustomImageView imageView_cust = new CustomImageView(
                mParentActivity
        );
        imageView_cust.setLayoutParams((new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                , ViewGroup.LayoutParams
                .MATCH_PARENT)));
        constraintLayout.addView(imageView_cust);
        return imageView_cust;
    }

}
