package eu.niko.plugin_manager.plugininitialization;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import eu.niko.naswi.aidl.Action;
import eu.niko.plugin_manager.ViewAdapter;
import eu.niko.pluginmanager.IParentActivity;
import eu.niko.plugin_manager.plugindrawhandler.Utility;

public class InitPluginMain {
    public static void generateControl(ViewAdapter viewAdapter , IParentActivity fragmentActivity
            , Object
                                               jsonObject , Action
                                               action)   {
        if (jsonObject instanceof JSONObject) {
            if (jsonObject instanceof JSONObject) {
                Gson gson
                        = new Gson();
                Utility.BaseControl baseControl = gson.fromJson(jsonObject.toString() , Utility.BaseControl.class);
                viewAdapter.loadViewAdapter(fragmentActivity , action , baseControl , baseControl
                        .getControlList().get
                        (0));
            }

        }

    }

    private static JSONObject getConfigFromAssets(Context context) {
        try {
            InputStream inputStream = context.getAssets().open("actionconfig.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);

            inputStream.close();

            String json = new String(buffer , "UTF-8");
            return new JSONObject(json);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void parseJSON(ViewAdapter viewAdapter, Action action , Context context) {
        try {
            JSONObject jsonObject = getConfigFromAssets(context);
            generateControl(viewAdapter,(IParentActivity) context , jsonObject.get(action
                            .getModel()) ,
                    action);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
