package eu.niko.plugin_manager;

import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Shader;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import eu.niko.pluginmanager.IClassLoader;
import eu.niko.pluginmanager.IParentActivity;
import com.sdsmdg.harjot.vectormaster.VectorMasterDrawable;
import com.sdsmdg.harjot.vectormaster.models.PathModel;

public class nextfragment extends Fragment implements IClassLoader, View.OnTouchListener {
    FragmentActivity mParentActivity;
    Region clickableRegion;
    VectorMasterDrawable vectorMasterDrawable;

    public static   nextfragment newInstance(IParentActivity parentActivity) {

        Bundle args = new Bundle();

        nextfragment fragment = new nextfragment();
        fragment.setArguments(args);
        fragment.loadedClass(parentActivity);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main , null);


        vectorMasterDrawable = new VectorMasterDrawable(mParentActivity , R.drawable.ic_swipe_dimmer);
        AppCompatImageView view1 = view.findViewById(R.id.vwTest);
        view1.setBackground(vectorMasterDrawable);
        view1.setOnTouchListener(this);
        PathModel pathModel = vectorMasterDrawable.getPathModelByName("xxx");
        clickableRegion = new Region();
        rectF = new RectF();
        Path bigCirclePath =
                pathModel.getPath();
        bigCirclePath.computeBounds(rectF , true);

        Rect rect = new Rect((int) rectF.left , (int) rectF.top , (int) rectF.right , (int) rectF
                .bottom);

        clickableRegion.setPath(bigCirclePath , new Region(rect));


        return view;
    }

    void buttonGradient(float f) {
        //  f*=.2;
        DisplayMetrics dm = mParentActivity.getResources().getDisplayMetrics();
        float densityDpi = dm.density;
        PathModel pathModel = vectorMasterDrawable.getPathModelByName("xxx");
        LinearGradient shader = new LinearGradient(0 , 0 , 0 , rectF.height()*densityDpi ,
                new int[]{Color.parseColor("#FDDE92") ,

                        Color.parseColor("#FFC73C")} , new float[]{(f - .001f)//-.01f for space
                // between gradient
                , f} , Shader
                .TileMode
                .CLAMP);

        Paint paint = new Paint();
        paint.setShader(shader);

        pathModel.setPathPaint(paint);
        vectorMasterDrawable.update();
    }

    RectF rectF;

    public boolean contains(float x , float y) {
        return clickableRegion.contains((int) x , (int) y);
    }

    @Override
    public Object loadedClass(Object... parentActivity) {
        mParentActivity = (FragmentActivity) parentActivity[0];
        return this;
    }

    @Override
    public Object talkToMe(Object... messages) {
        return null;
    }


    @Override
    public boolean onTouch(View view , MotionEvent motionEvent) {

        DisplayMetrics dm = getResources().getDisplayMetrics();

        float x1 = motionEvent.getX() * (160f / dm.densityDpi);
        float y1 = motionEvent.getY() * (160f / dm.densityDpi);

        if (contains(x1 , y1)) {
            // Toast.makeText(getActivity(),String.format("%s,%s",x1,y1),Toast.LENGTH_SHORT).show();
            int act=motionEvent.getAction();
            switch (motionEvent.getActionMasked()) {
                case MotionEvent.ACTION_MOVE:
                    float a = y1 / (rectF.bottom - rectF.top);
                    scale(a);
                    String g = "";
                    return true;

            }
            return true;// mGestureDetector.onTouchEvent(motionEvent);
        }

        return true;
    }


    void scale(float y) {
        buttonGradient(y);

    }

    boolean isOn = true;
}
