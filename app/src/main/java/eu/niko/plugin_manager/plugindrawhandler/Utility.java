package eu.niko.plugin_manager.plugindrawhandler;

import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Region;
import android.graphics.Shader;

import com.google.gson.Gson;
import com.sdsmdg.harjot.vectormaster.VectorMasterDrawable;
import com.sdsmdg.harjot.vectormaster.models.PathModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import eu.niko.naswi.aidl.Property;

public class Utility {
    public  String getBaseJSON(String jLevel) {
        Gson gson = new Gson();
        BaseControl baseControl = new BaseControl();
        baseControl.name = "light";
        baseControl.controlList = new ArrayList<>();

        Control kontrol = gson.fromJson(jLevel , Control.class);

        Control control = new Control();
        control.drawer = new Drawer("ic_tap_lightbulb" , "xxx");
        control.isActive = true;
        control.className = "eu.niko.plugin_manager.ControlUiHandler.TapControl";

        control.responses = new Response[]{
                new Response("On" ,
                        new ModelPath("xxx" , "#FFC73C") ,
                        new ModelGroup("changeGroup" , "#725100")
                ) ,
                new Response("On" ,
                        new ModelPath("xxx" , "#4C4C4C") ,
                        new ModelGroup("changeGroup" , "#191919")
                )
        };
        control.touchEvents = new TouchEvent[]{
                new TouchEvent("tap" , new Prop[]{
                        new Prop("Status" , "On") ,
                        new Prop("Status" , "Off")
                })

        };
        baseControl.controlList.add(kontrol);
        baseControl.hasLevel = baseControl.controlList.size() > 1;
        return gson.toJson(baseControl);
    }

    public static void paintGradient(VectorMasterDrawable vectorMasterDrawable , Region region ,
                                     float
                                             percent ,
                                     float densityDpi) {
        try {

            PathModel pathModel = vectorMasterDrawable.getPathModelByName("xxx");
            LinearGradient shader = new LinearGradient(0 , 0 , 0 , region
                    .getBounds().height()
                    * densityDpi ,
                    new int[]{Color.parseColor("#FDDE92") ,
                            Color.parseColor("#FFC73C")} , new float[]{(percent - .01f)
                    // for
                    // space
                    // between gradient
                    , percent} , Shader
                    .TileMode
                    .CLAMP);
            Paint paint = new Paint();
            paint.setShader(shader);
            pathModel.setPathPaint(paint);
            vectorMasterDrawable.update();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public class BaseControl implements Serializable {
        private Property[] properties;
        private String name;
        private List<Control> controlList;
        private boolean hasLevel = false;

        public String getName() {
            return name;
        }

        public List<Control> getControlList() {
            return controlList;
        }

        public boolean isHasLevel() {
            return hasLevel;
        }

        public Property[] getProperties() {
            return properties;
        }

        public void setProperties(Property[] properties) {
            this.properties = properties;
        }
    }

    public class Control implements Serializable {
        private boolean isActive;
        private String className;
        private Drawer drawer;
        private Response[] responses;
        private TouchEvent[] touchEvents;

        public String getClassName() {
            return className;
        }

        public Drawer getDrawer() {
            return drawer;
        }

        public Response[] getResponses() {
            return responses;
        }

        public TouchEvent[] getTouchEvents() {
            return touchEvents;
        }


    }

    public class Prop implements Serializable {
        public Prop(String name , String value) {
            this.name = name;
            this.value = value;
        }

        String name, value;
        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }

    public class TouchEvent implements Serializable {
        public TouchEvent(String name , Prop[] props) {
            this.name = name;
            this.props = props;
        }

        private String name;
        private Prop[] props;

        public String getName() {
            return name;
        }

        public Prop[] getProps() {
            return props;
        }
    }

    public class ModelPath implements Serializable {
        public ModelPath(String name , String color) {
            this.name = name;
            this.color = color;
        }

        private String name, color;
        public String getName() {
            return name;
        }

        public String getColor() {
            return color;
        }
    }

    public class ModelGroup implements Serializable {
        public ModelGroup(String name , String color) {
            this.name = name;
            this.color = color;
        }

        String name, color;

        public String getName() {
            return name;
        }

        public String getColor() {
            return color;
        }
    }

    public class Response implements Serializable {
        public Response(String name , ModelPath pathModel , ModelGroup groupModel) {
            this.groupModel = groupModel;
            this.pathModel = pathModel;
            this.name = name;

        }

        private String name;
        private ModelPath pathModel;
        private ModelGroup groupModel;

        public ModelGroup getGroupModel() {
            return groupModel;
        }

        public ModelPath getPathModel() {
            return pathModel;
        }

        public String getName() {
            return name;
        }
    }

    public class Drawer implements Serializable {
        public Drawer(String drawableId , String activeRegion) {
            this.drawableId = drawableId;
            this.activeRegion = activeRegion;
        }

        private String drawableId;
        private String activeRegion;

        public String getDrawableId() {
            return drawableId;
        }

        public String getActiveRegion() {
            return activeRegion;
        }
    }

}