package eu.niko.plugin_manager;
import android.graphics.Region;
import android.support.v7.widget.AppCompatImageView;
import android.view.GestureDetector;
import android.view.View;

import eu.niko.plugin_manager.ControlUiHandler.GestureListener;
import eu.niko.plugin_manager.plugindrawhandler.Utility;

public interface IPlayer {
    void initialize(FragmentHandler fragmentHandler);
    void initialize(FragmentHandler fragmentHandler, int resId,int vectorId,Utility.Control control );
    void initialize(FragmentHandler fragmentHandler, AppCompatImageView imageView, int vectorId, Utility.Control
            control );
    View drawView(int resId,int vectroId);
    Region defineClickableRegion(String vectorPathName);
    GestureDetector defineGesture(GestureListener gestureListener);
}
