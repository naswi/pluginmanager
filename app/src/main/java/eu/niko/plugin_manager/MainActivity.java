package eu.niko.plugin_manager;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import eu.niko.plugin_manager.plugindrawhandler.Utility;
import eu.niko.pluginmanager.IParentActivity;
import eu.niko.pluginmanager.IViewAdapter;

import eu.niko.naswi.aidl.Action;
import eu.niko.naswi.aidl.Property;

public class MainActivity extends FragmentActivity implements IParentActivity {
    ViewAdapter viewAdapter;
    public Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            talkToMe(message.obj);
            return false;
        }
    });

    void parseJSON(Action action) {
        try {
            InputStream inputStream = getAssets().open("actionconfig.json");
            int size = inputStream.available();

            byte[] buffer = new byte[size];

            inputStream.read(buffer);

            inputStream.close();

            String json = new String(buffer , "UTF-8");
            JSONObject jsonObject = new JSONObject(json);
            generateControl(jsonObject.get("dimmer") , action);
            generateControl(jsonObject.get("light") , action);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void generateControl(Object jsonObject , Action action) throws JSONException {
        if (jsonObject instanceof JSONObject) {
            Gson gson
                    = new Gson();
            Utility.BaseControl baseControl=gson.fromJson(jsonObject.toString(),Utility.BaseControl.class);
            viewAdapter.loadViewAdapter(this , action , baseControl,baseControl.getControlList().get
                    (0));
        }

    }

    public static void loopThroughJson(Object jsonObject) throws JSONException {
        Iterator<String> keys = null;
        if (jsonObject instanceof JSONObject)
            keys = ((JSONObject) jsonObject).keys();
        while (keys != null && keys.hasNext()) {
            String jString = keys.next();
            Object jObj = ((JSONObject) jsonObject).get(jString);
            if (jObj instanceof JSONArray) {
                JSONArray jsonArray = (JSONArray) jObj;
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject j = jsonArray.getJSONObject(i);
                    loopThroughJson(j);
                }
            } else {
                loopThroughJson(jObj);
            }
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewPager viewPager = findViewById(R.id.frmT);
        viewAdapter = new ViewAdapter(this.getSupportFragmentManager());
        Action action = new Action(null , null , "dimmer" , "131231231kkjldsf" , null , null);
        parseJSON(action);
        viewPager.setPadding(20 , 0 , 20 , 0);
        viewPager.setAdapter(viewAdapter);
        viewPager.setClipChildren(false);
        viewPager.setPageMargin(150);
        viewPager.setOffscreenPageLimit(6);
        PluginMain.viewAdapter=viewAdapter;


    }

    @Override
    public void onAttachFragment(Fragment fragment1) {
        super.onAttachFragment(fragment1);

    }

    @Override
    public void talkToMe(Object... object) {
        try {
            Object[] k=(Object[]) object[0];
            Log.i("Fucker" , "talkToMe: ");
            Property[] properties = (Property[]) k[1];
            for (Fragment fragment : viewAdapter.fragments) {
                FragmentHandler fragmentHandler = (FragmentHandler) fragment;

                if (fragmentHandler.action.getUuid().equals("131231231kkjldsf")) {
                    fragmentHandler.doAction(properties);
                    break;

                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void freeResource() {

    }

    @Override
    public void startActivity(Object... objects) {

    }

    @Override
    public IViewAdapter setViewAdapter() {
        return null;
    }


}



