package eu.niko.plugin_manager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.Pair;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import eu.niko.pluginmanager.IClassLoader;
import eu.niko.pluginmanager.IParentActivity;

import eu.niko.naswi.aidl.Action;
import eu.niko.naswi.aidl.Property;

import static eu.niko.plugin_manager.plugininitialization.InitPluginMain.parseJSON;

public class PluginMain implements IClassLoader {
   static ViewAdapter viewAdapter;

    public static ViewAdapter getViewAdapter() {
        return viewAdapter;
    }

    @Override
    public Object loadedClass(Object... parentActivity) {
        IParentActivity fragmentActivity = (IParentActivity) parentActivity[0];
        Action[] actions = (Action[]) parentActivity[1];
        viewAdapter = new ViewAdapter(((FragmentActivity) parentActivity[0])
                .getSupportFragmentManager());
        for (Action action : actions
                ) {
            parseJSON(viewAdapter,action , (Context) fragmentActivity);
        }
        return viewAdapter;
    }




    @Override
    public Object talkToMe(Object... messages) {

        if (messages != null && messages.length == 0)
            return null;
        Pair<Action, Property[]> pair = (Pair<Action, Property[]>) messages[0];
        Action action = pair.first;
        Property[] properties
                = pair.second;
        if (viewAdapter.fragments.size() > 0)
            for (Fragment fragment : viewAdapter.fragments) {
                FragmentHandler fragmentHandler = (FragmentHandler) fragment;
                if (fragmentHandler.action != null && fragmentHandler.action.getUuid().equals(action
                        .getUuid())) {
                    Log.i("XXX89" , "talkToMe: " + messages.length);
                    fragmentHandler.doAction(properties);
                    break;

                }
            }


        return null;
    }
}
