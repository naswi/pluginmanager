package eu.niko.plugin_manager;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.ArrayList;

import eu.niko.naswi.aidl.Action;
import eu.niko.plugin_manager.plugindrawhandler.Utility;
import eu.niko.pluginmanager.IParentActivity;
import eu.niko.pluginmanager.IViewAdapter;

public class ViewAdapter extends FragmentStatePagerAdapter implements IViewAdapter {

    int pageCount = 0;
    ArrayList<Fragment> fragments = new ArrayList<>();
    IParentActivity mParentActivity;
    boolean isReady = false;

    public ArrayList<Fragment> getFragments() {
        return fragments;
    }

    public ViewAdapter(FragmentManager fm) {
        super(fm);
    }

    public void loadViewAdapter(IParentActivity parentActivity , Action controlName ,
                                Utility.BaseControl kontrol , Utility.Control control) {
        mParentActivity = parentActivity;

        fragments.add(FragmentHandler.newInstance(parentActivity , controlName , kontrol ,
                control , pageCount));
        pageCount++;


    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
        //return super.getItemPosition(object);
    }

    public void loadViewAdapter(IParentActivity parentActivity , Action[] controlNames) {
        mParentActivity = parentActivity;
        pageCount = controlNames.length;
        int i = 0;
        for (Action controlName : controlNames) {
            i++;
            Log.i("XXX0" , "loadViewAdapter: " + controlName.getName());
            fragments.add(FragmentHandler.newInstance(parentActivity , controlName , i));
        }


    }

    int currentFragId = -1;

    @Override
    public Fragment getItem(int i) {
        if (!isReady) {
            Log.i("XXXMM" , "getItem: " + isReady);
            Class<?> clazz = mParentActivity.getClass();
            Field field = null;
            try {
                field = clazz.getDeclaredField("isReady");
                field.setAccessible(true);
                field.setBoolean(mParentActivity , true);

            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        currentFragId = i;
        return fragments.get(i);
    }

    @Override
    public float getPageWidth(int position) {
        return super.getPageWidth(position);
    }

    @Override
    public int getCount() {
        return pageCount;
    }

}
