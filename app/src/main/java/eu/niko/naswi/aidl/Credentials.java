package eu.niko.naswi.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public class Credentials implements Parcelable {

    public static final Creator<Credentials> CREATOR
            = new Creator<Credentials>() {
        public Credentials createFromParcel(Parcel in) {
            return new Credentials(in.readString(), in.readString());
        }

        public Credentials[] newArray(int size) {
            return new Credentials[size];
        }
    };

    private final String user;
    private final String password;

    public Credentials(String user, String password) {
        this.user = user;
        this.password = password;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(user);
        dest.writeString(password);
    }
}
