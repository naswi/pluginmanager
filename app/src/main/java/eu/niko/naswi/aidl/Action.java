package eu.niko.naswi.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public class Action implements Parcelable {
    public static final Creator<Action> CREATOR
            = new Creator<Action>() {
        public Action createFromParcel(Parcel in) {
            return new Action(in.readString(), // type
                    in.readString(), // technology
                    in.readString(), // model
                    in.readString(), // uuid
                    in.readString(), // name
                    in.readString()); // locationId
        }

        public Action[] newArray(int size) {
            return new Action[size];
        }
    };

    private final String type;
    private final String technology;
    private final String model;
    private final String uuid;
    private final String name;
    private final String locationId;

    public Action(String type, String technology, String model, String uuid, String name, String locationId) {
        this.type = type;
        this.technology = technology;
        this.model = model;
        this.uuid = uuid;
        this.name = name;
        this.locationId = locationId;
    }

    public String getType() {
        return type;
    }

    public String getTechnology() {
        return technology;
    }

    public String getModel() {
        return model;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getLocationId() {
        return locationId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(technology);
        dest.writeString(model);
        dest.writeString(uuid);
        dest.writeString(name);
        dest.writeString(locationId);
    }
}
