package eu.niko.naswi.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public class Location implements Parcelable {

    public static final Creator<Location> CREATOR
            = new Creator<Location>() {
        public Location createFromParcel(Parcel in) {
            return new Location(in.readString(), in.readString());
        }

        public Location[] newArray(int size) {
            return new Location[size];
        }
    };

    private final String name;
    private final String uuid;

    public Location(String name, String uuid) {
        this.name = name;
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public String getUuid() {
        return uuid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(uuid);
    }
}
