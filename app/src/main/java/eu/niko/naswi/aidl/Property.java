package eu.niko.naswi.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public class Property implements Parcelable {

    public static final Creator<Property> CREATOR
            = new Creator<Property>() {
        public Property createFromParcel(Parcel in) {
            return new Property(in.readString(), in.readString());
        }

        public Property[] newArray(int size) {
            return new Property[size];
        }
    };

    private final String name;
    private final String value;

    public Property(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(value);
    }
}
